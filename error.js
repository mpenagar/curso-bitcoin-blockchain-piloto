class Error {
  constructor(type, message) {
    this.type = type
    this.message = message
  }
}

module.exports = Error
