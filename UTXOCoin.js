// transaccion con output P2PK https://blockchain.info/rawtx/4a5e1e4baab89f3a32518a88c31bc87f618f76673e2cc77ab2127b7afdeda33b
// transaccion con dos outputs P2PKH https://blockchain.info/rawtx/6359f0868171b1d194cbee1af2f16ea598ae8fad666d9b012c8ed2b79a236ec4;
// http://chainquery.com/bitcoin-api/decodescript para ver scriptSig y scriptPubKey
const secp256k1 = require('secp256k1')
const Utils = require('./Utils')
const Genesis = require('./genesis.json')[0]
const Script = require('./script')
const BtcProof = require('bitcoin-proof')
const Error = require('./error')

const { bank } = Utils.makeBankAnd2address()

class Block {
  constructor (header, transactions, pubkey = bank.pubkey.toString('hex'), signature = '', merkleRoot = '', nonce = 0) {
    this.header = {}
    this.header.index = header.index
    this.header.timestamp = header.timestamp
    this.header.previousHash = ''
    this.header.merkleRoot = merkleRoot
    this.header.version = 1
    this.header.difficulty = 2
    this.header.target = 16 ** (64 - this.header.difficulty)
    this.header.nonce = nonce
    this.transactions = transactions
    this.pubkey = pubkey
    this.signature = signature
    
    this.hash = this.calculateHash()
    this._num = parseInt(this.hash, 16)
  }
  
  getMerkleRoot(txs) {
    let _txs = []
    
    if (txs.length < 1) return []

    txs.map(tx => _txs.push(tx.txid))

    return BtcProof.getMerkleRoot(_txs)
  }
  
  calculateHash() {
    const str = JSON.stringify(this.header)
    
    return Utils.sha256(str)
  }
  
  signBlock(privkey) {
    privkey = Buffer.from(privkey, 'hex')
    
    const str = JSON.stringify(this.header) +
    JSON.stringify(this.transactions) + this.pubkey + this.hash
    const msg = Buffer.from(Utils.sha256(str), 'hex')
    
    return secp256k1.sign(msg, privkey)
  }
  
  mineBlock() {
    while (this.header.target < this._num) {
      this.header.nonce++
      this.hash = this.calculateHash()
      this._num = parseInt(this.hash, 16)
    }
  }
}

class Transaction {
  constructor() { 
    this.input = []
    this.output = []
    this.locktime = ''
    this.txid = '' // 64 bytes
    this.coinbase = false
  }

  createtx(prevTx, output, blockchain, noSign = false, time = Genesis.transactions[0].locktime) {
    try {
      this.isWalletBalance(prevTx, output)
    } catch (err) {
      return err
    }

    let input = ''

    this.locktime = time
    if (prevTx.hasOwnProperty('sequience')) {
      input = prevTx
      this.coinbase = true
    } else {
      input = blockchain.utxo.find(u => u.txid == prevTx.txid && u.puntero[0].n == prevTx.puntero[0].n)
    }
    this.input.push(input)
    
    this.output.push({
      'amount': output.value,
      'n': output.n,
      'scriptPubKey': `OP_DUP OP_HASH160 ${Utils.fromPubKeyToPubKeyHash(output.address)} OP_EQUALVERIFY OP_CHECKSIG`,
    })
    this.addChange(input, output.value, output.n, output.fee, blockchain.subsidy)
    
    if (noSign) this.txid = this.calculateTxid()

    return true
  }
  
  addChange (input, value, n, fee, subsidy) {
    let inputBalance = 0
    let inputAddressHash = undefined
    let changeBalance = 0
    
    if (input.hasOwnProperty('sequience')) {
      // console.log('coinbase tx', subsidy)
      inputBalance += subsidy
      inputAddressHash = Utils.fromPubKeyToPubKeyHash(bank.pubkey)
    } else {
      // console.log('no coinbase tx', input.puntero[0].amount)
      inputBalance += input.puntero[0].amount
      inputAddressHash = input.puntero[0].scriptPubKey.split(' ')
      inputAddressHash = inputAddressHash[2]
    }
    
    changeBalance = inputBalance - value - fee

    if (changeBalance > 0) {
      this.output.push({
        'amount': changeBalance,
        'n': n + 1,
        'scriptPubKey': `OP_DUP OP_HASH160 ${inputAddressHash} OP_EQUALVERIFY OP_CHECKSIG`
      })
    }

    return true
  }
  
  signTx(privkey) {
    privkey = Buffer.from(privkey, 'hex')
    const pubkey = secp256k1.publicKeyCreate(privkey)
    
    const str = this.input[0].txid + this.input[0].puntero[0].amount +
    this.input[0].puntero[0].n + this.input[0].puntero[0].scriptPubKey +
    this.input[0].puntero[0].address + JSON.stringify(this.output) +
    this.locktime + pubkey.toString('hex')

    // console.log(' --------------- str on utxocoin', str)
    const msg = Buffer.from(Utils.sha256(str), 'hex')
    
    const { signature } = secp256k1.sign(msg, privkey)
    this.input[0].scriptSig = `${signature.toString('hex')} ${pubkey.toString('hex')}`
    this.txid = this.calculateTxid()
  }

  calculateTxid() {
    const str = JSON.stringify(this.input) + JSON.stringify(this.output) + this.locktime
    return Utils.sha256(str)
  }

  isWalletBalance(prevTx, output) {
    if(!prevTx.hasOwnProperty('sequience') && prevTx.puntero[0].amount < output.value + output.fee) {
      throw new Error('less-balance', 'Invalid tx: less balance than want to send + fee')
    }

    return true
  }
}

class Blockchain {
  constructor() {
    this.chain = []
    this.mempool = []
    this.subsidy = 50
    this.utxo = []
    
    const genesisBlock = this.createGenesisBlock(true)
    this.chain.push(genesisBlock)
  }
  
  createGenesisBlock(makeUtxo = false) {
    const genesisTx = new Transaction()
    const _GTxInput = Genesis.transactions[0].input[0]
    const _GTxOutput = Genesis.transactions[0].output[0]
    const blockHeader = { index: Genesis.header.index, timestamp: Genesis.header.timestamp }
    
    genesisTx.createtx(_GTxInput, { address: _GTxOutput.address, value: _GTxOutput.amount, n: _GTxOutput.n, fee: 0 }, this, true)
    const genesisBlock = new Block(blockHeader, [genesisTx], Genesis.pubkey, Genesis.signature, Genesis.header.merkleRoot, Genesis.header.nonce)
    // genesisBlock.mineBlock()
    // console.log('genesisBlock', genesisBlock)

    if (makeUtxo) {
      this.utxo.push({
        txid: genesisTx.txid,
        puntero: genesisTx.output,
        coinbase: true
      })
    }
    
    return genesisBlock
   }
  
  getLastBlock() {
    return this.chain[this.chain.length - 1]
  }
  
  addBlock(newBlock, privkey = require('./bankWallet.json').privkey) {
    const blockFees = this.getBlockFee(newBlock.transactions)
    const coinbaseTx = this.addCoinbase(bank.pubkey, blockFees)
    newBlock.transactions.unshift(coinbaseTx)
    newBlock.header.previousHash = this.getLastBlock().hash
    newBlock.header.merkleRoot = newBlock.getMerkleRoot(newBlock.transactions)
    
    newBlock.mineBlock()
    this.mempool = []
    
    const { signature } = newBlock.signBlock(privkey)
    newBlock.signature = signature.toString('hex')

    const isBlockValid = ourbank.isBlockValid(newBlock)
    if (typeof isBlockValid === 'object' ) {
      // console.log('Bloque no válido. Se olvida. No se almacena')
      return isBlockValid
    }
    
    this.chain.push(newBlock)
    
    newBlock.transactions.map(t => {
      t.input.map((input, n) => {
        if (!input.hasOwnProperty('sequience')) {
          this.updateUTXO(input.txid, n)
        } 
      })
        
      t.output.map(output => {
        this.utxo.push({
          txid: t.txid,
          puntero: [ output ],
          coinbase: t.coinbase 
        })
      })
    })

    return true
  } 
  
  updateUTXO(txid, n) {
    let index = this.utxo.findIndex(u => u.txid == txid && u.puntero[0].n === n)

    if (index > -1) {
      // console.log('index', index)
      this.utxo.splice(index, 1)
    }
  }

  getBlockFee(transactions) {
    let blockFees = 0
    
    transactions.map(tx => {
      let inputAmount = 0
      let outputAmount = 0

      if (!tx.input[0].hasOwnProperty('sequience')) {
        inputAmount = tx.input[0].puntero[0].amount
      }

      for (const output of tx.output) {
        outputAmount += output.amount
      }

      blockFees += inputAmount - outputAmount
    })

    return blockFees
  }
  
  addCoinbase(pubkey, fees) {
    const addressHash = Utils.fromPubKeyToPubKeyHash(pubkey.toString('hex'))

    const input = {
      // 'coinbase': '03443b040385402062f503253482f',
      'sequience': 4294967295,
      'scriptSig': 'put-your-script'
    }

    const output = { 
      value: this.subsidy + fees,
      n: 0,
      address: addressHash,
      fee: 0
    }
    const coinbaseTx = new Transaction()
    coinbaseTx.createtx(input, output, this, true, Utils.now())

    return coinbaseTx
  }

  getBalance(pubkey) {
    let balance = 0
    for (const block of this.chain) {
      for (const tx of block.transactions) {
        
        if (tx.coinbase && pubkey === bank.pubkey.toString('hex')) {
          // console.log('coinbase')
          balance += tx.output[0].amount
        } else if (!tx.coinbase) {
          const scriptPubKey = tx.input[0].puntero[0].scriptPubKey.split(' ')
          if (scriptPubKey[2] === Utils.fromPubKeyToPubKeyHash(pubkey.toString('hex'))) {
            // console.log('minus')
            balance -= tx.input[0].puntero[0].amount
          }
          
          // console.log('block', tx.output[0].address, address)
          for (const output of tx.output) {
            const outputScriptPubKey = output.scriptPubKey.split(' ')
            if (outputScriptPubKey[2] === Utils.fromPubKeyToPubKeyHash(pubkey.toString('hex'))) {
              // console.log('plus')
              balance += output.amount
            }
          }
        }
      }
    }
    
    return balance
  }
}

// Node on the next classes
class Bank {
  constructor() {
    this.blockchain = new Blockchain()
  }
  
  isBlockValid(newBlock) {
    const previousBlock = this.blockchain.getLastBlock()
    
    // console.log("previousBlock", previousBlock)
    // console.log("currentBlock", newBlock)
    
    return this.validations(newBlock, previousBlock)
  }
  
  validations(newBlock, previousBlock) {
    try {
      const str = JSON.stringify(newBlock.header) +
      JSON.stringify(newBlock.transactions) + newBlock.pubkey +
      newBlock.hash
      const msg = Buffer.from(Utils.sha256(str),'hex')

      const signature = Buffer.from(newBlock.signature, 'hex')
      const pubkey = Buffer.from(newBlock.pubkey, 'hex')

      this.isValidBlockHash(newBlock)
      this.isValidPreviousHash(newBlock, previousBlock)
      this.isValidMerkleRoot(newBlock)
      this.isValidSig(msg, signature, pubkey)
      this.isValidBankSig(msg, signature, bank.pubkey)
      this.isValidProof(newBlock)
      this.areTxsValid(newBlock)
    
      return true
    } catch (err) {
      // console.log('ERROR', err)
      return err
    }
  }

  // https://bitcoin.stackexchange.com/a/8813
  makeScript(tx) {
    const scriptSig = tx.input[0].scriptSig.split(' ')
    
    let splittedPubKey = tx.output[0].scriptPubKey.split(' ')

    // splittedPubKey[2] = '6146b8234f71463122d032accd15c6925fa0c4f2'
    splittedPubKey[2] = tx.input[0].puntero[0].scriptPubKey.split(' ')[2]
    
    return scriptSig.join(' ') + ' ' + splittedPubKey.join(' ')
  }

  isValidGenesisBlock() {
    const memoryGenesisBlock = JSON.stringify(this.blockchain.chain[0])
    const genesisBlock = JSON.stringify(this.blockchain.createGenesisBlock())
    
    if (memoryGenesisBlock !== genesisBlock) {
      throw new Error('invalid-genesisblock', 'Invalid block: genesis block')
    }

    return true
  }
  
  isValidMerkleRoot(block) {
    if (block.getMerkleRoot(block.transactions) !== block.header.merkleRoot) {
      throw new Error('invalid-merkleroot', 'Invalid block: merkleRoot')
    }

    return true
  }

  isValidPreviousHash(newBlock, previousBlock) {
    if (newBlock.header.previousHash !== previousBlock.calculateHash()) {
      throw new Error('invalid-hashpreviousblock', 'Invalid block: hashPreviousBlock')
    }

    return true
  }

  isValidBlockHash(newBlock) {
    if (newBlock.hash !== newBlock.calculateHash()) {
      throw new Error('invalid-block-hash', 'Invalid block: block hash')
    }

    return true
  }

  isValidSig(msg, signature, pubkey) {
    if (!secp256k1.verify(msg, signature, pubkey)) {
      throw new Error('not-signed-by.correct-privkey', 'Not signed by the correct privKey')
    }

    return true
  }

  isValidBankSig(msg, signature, bankpubkey) {
    if (!secp256k1.verify(msg, signature, bankpubkey))   {
      throw new Error('not-signed-by.bank', 'Not signed by the bank')
    }

    return true
  }
  
  areTxsValid(block) {
    if (block.transactions.length > 0) {
      return block.transactions.map(tx => {
        this.isValidTxId(tx)
        this.isValidTxLocktime(tx)
        
        if(!tx.coinbase) {
          const script = this.makeScript(tx)
          const stack = new Script(script, tx)
          
          const index = this.blockchain.utxo.findIndex(utxo => utxo.txid === tx.input[0].txid && utxo.puntero[0].n === tx.input[0].puntero[0].n)
          this.isValidScript(stack)
          this.isValidUTXO(index)
          this.isBalance(tx)
        }
      })
    }

    return true
  }

  isValidTxId(tx) {
    if (tx.txid !== tx.calculateTxid()) {
      throw new Error('invalid-tx-id', 'Invalid transaction id/hash')
    }

    return true
  }

  isValidTxLocktime(tx) {
    if (Utils.now() < tx.locktime) {
      throw new Error('invalid-tx-locktime', 'Invalid transaction locktime')
    }
    
    return true
  }

  isBalance(tx) {
    const balance = tx.input[0].puntero[0].amount  
    const toSend = tx.output[0].amount
    const fee =  balance - toSend

    if (balance < toSend + fee) {
      throw new Error('invalid-balance', 'Invalid balance')
    }

    return true
  }

  isValidProof(newBlock) {
    if (parseInt(newBlock.hash, 16) >= newBlock.header.target) {
      throw new Error('not-valid-proof', 'Invalid block: not valid proof')
    }

    return true
  }

  isValidScript(stack) {
    if (stack.stack.length !== 1 && stack.stack[0] != true) {
      throw new Error('id-tx-invalid', 'id transaction invalid')
    }

    return true
  }

  isValidUTXO (index) {
    if (index === -1) {
      throw new Error('no-utxo', 'No UTXO')
    }

    return true
  }
}

module.exports.Blockchain = Blockchain
module.exports.Block = Block
module.exports.Transaction = Transaction
module.exports.Bank = Bank
