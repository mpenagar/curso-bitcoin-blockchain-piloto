const Crypto = require('crypto')
const Utils = require('./Utils')
const genesisTimestamp = Utils.now()
const secp256k1 = require('secp256k1')

 class Block {
  constructor (header, transactions, pubkey = bank.bankPubKeyHex, signature = '') {
    this.header = {}
    this.header.index = header.index
    this.header.timestamp = header.timestamp
    this.header.previousHash = ''
    this.header.difficulty = 2
    this.header.target = 16 ** (64 - this.header.difficulty)
    this.header.nonce = 0
    this.transactions = transactions
    this.pubkey = pubkey
    this.signature = signature

    this.hash = this.calculateHash()
    this._num = parseInt(this.hash, 16)
  }

  calculateHash() {
    const str = JSON.stringify(this.header)

    return Utils.sha256(str)
  }

  signBlock(privkey) {
    privkey = Buffer.from(privkey, 'hex')

    const str = JSON.stringify(this.header) +
    JSON.stringify(this.transactions) + this.pubkey + this.hash
    const msg = Buffer.from(Utils.sha256(str), 'hex')
    
    return secp256k1.sign(msg, privkey)
  }

  mineBlock() {
    while (this.header.target < this._num) {
      this.header.nonce++
      this.hash = this.calculateHash()
      this._num = parseInt(this.hash, 16)
    }
  }
}

class Transaction {
  constructor(from, to, amount, locktime) {
    this.from = from
    this.to = to
    this.amount = amount
    this.locktime = locktime

    this.txid = this.calculateTxid()
  }

  calculateTxid() {
    const str = this.from + this.to + this.amount + this.locktime
    return Utils.sha256(str)
  }
}

class Blockchain {
  constructor() {
    this.chain = []
    this.mempool = []
    this.subsidy = 50

    const genesisBlock = this.createGenesisBlock()
    this.chain.push(genesisBlock)
  }

  createGenesisBlock() {
    const genesisTx = new Transaction(null, 'miner', this.subsidy, genesisTimestamp)
    
    return new Block({ index: 0, timestamp: genesisTimestamp }, [genesisTx], 'pubkey', 'signature')
  }

  getLastBlock() {
    return this.chain[this.chain.length - 1]
  }

  addBlock(newBlock, privkey = bank.bankPrivKey) {
    const coinbaseTx = new Transaction(null, 'miner', this.subsidy, Utils.now())
    
    newBlock.transactions.unshift(coinbaseTx)
    newBlock.header.previousHash = this.getLastBlock().hash
    newBlock.mineBlock()
    this.mempool = []
    
    const { signature } = newBlock.signBlock(privkey)
    newBlock.signature = signature.toString('hex')
    
    if (bank.isBlockValid(newBlock)) {
      this.chain.push(newBlock)
    } else {
      console.log('Bloque no válido. Se olvida. No se almacena')
    }
  }
  
  /*
  / txid nos da la ultima transaccion de la cadena que queremos leer.
  / Para defecto queremos ver el balance de nuestras monedas al final de la cadena.
  / Menos cuando estamos validando la cadena que nos interesa leer la cadena hasta
  / la transaccion donde estamos.
  / txid is because when you are validating all the chain (isChainValid) and
  / trying to get the balance of an account, you need to read the chain 
  / until a tx.txid is finded. When you are validating a block (isValidBlock, try
  / to adding a new block to the chain) and trying to get the balance of an account, 
  / you need all the block.
  */
  getBalance(address, txid) {
    let balance = 0
    for (const block of this.chain) {
      for (const tx of block.transactions) {
        if (tx.txid === txid)  return balance
        
        if (tx.from === address) {
          balance -= tx.amount
        }

        if (tx.to === address) {
          balance += tx.amount
        }
      }
    }
    
    return balance
  }
}

// Node on the next classes
class Bank {
  constructor() {
    this.blockchain = new Blockchain()

    this.bankPrivKey = Crypto.randomBytes(32)
    this.bankPubKey = secp256k1.publicKeyCreate(this.bankPrivKey)
    this.bankPubKeyHex = this.bankPubKey.toString('hex')
  }

  isBlockValid(newBlock) {
    const previousBlock = this.blockchain.getLastBlock() 

    return this.validations(newBlock, previousBlock)
  }

  validations(newBlock, previousBlock) {
    try {
      const str = JSON.stringify(newBlock.header) +
      JSON.stringify(newBlock.transactions) + newBlock.pubkey +
      newBlock.hash
      const msg = Buffer.from(Utils.sha256(str),'hex')

      const signature = Buffer.from(newBlock.signature, 'hex')
      const pubkey = Buffer.from(newBlock.pubkey, 'hex')
    
      this.isValidBlockHash(newBlock)
      this.isValidPreviousHash(newBlock, previousBlock)
      this.isValidSig(msg, signature, pubkey)
      this.isValidBankSig(msg, signature, this.bankPubKey)
      this.areTxsValid(newBlock)
      return this.isValidProof(newBlock)
    } catch (err) {
      console.log('ERROR', err)
      return false
    }
  }
  
  isValidGenesisBlock() {
    const memoryGenesisBlock = JSON.stringify(this.blockchain.chain[0])
    const genesisBlock = JSON.stringify(this.blockchain.createGenesisBlock())
    
    if (memoryGenesisBlock !== genesisBlock) {
      throw 'Error: Invalid block: genesis block'
    }

    return true
  }

  isValidPreviousHash(newBlock, previousBlock) {
    if (newBlock.header.previousHash !== previousBlock.calculateHash()) {
      throw 'Error: Invalid block: hashPreviousBlock'
    }

    return true
  }

  isValidBlockHash(newBlock) {
    if (newBlock.hash !== newBlock.calculateHash()) {
      throw 'Error: Invalid block: block hash'
    }

    return true
  }

  isValidSig(msg, signature, pubkey) {
    if (!secp256k1.verify(msg, signature, pubkey)) {
      throw 'Error: not signed by the correct privKey'
    }

    return true
  }

  isValidBankSig(msg, signature, bankpubkey) {
    if (!secp256k1.verify(msg, signature, bankpubkey))   {
      throw 'Error: not signed by the bank'
    }

    return true
  }
  
  areTxsValid(block) {
    if (block.transactions.length > 0) {
      return block.transactions.map(tx => {
        if (!this.isTxValid(tx)) throw 'Error: Invalid transaction'
      })
    }
    
    return true
  }

  isTxValid(tx) {
    try {
      this.isValidTxId(tx)
      this.isValidTxLocktime(tx)
      if (tx.from === null) return true
      return this.isBalance(tx)
    } catch (err) {
      console.log('ERROR tx', err)
      return false
    }
  }

  isValidTxId(tx) {
    if (tx.txid !== tx.calculateTxid()) {
      throw 'Error: Invalid transaction id/hash'
    }

    return true
  }

  isValidTxLocktime(tx) {
    if (Utils.now() < tx.locktime) {
      throw 'Error: Invalid transaction locktine'
    }

    return true
  }

  isBalance(tx) {
    if (this.blockchain.getBalance(tx.from, tx.txid) < tx.amount) {
      throw 'Error: Invalid balance'
    }

    return true
  }

  isValidProof(newBlock) {
    if (parseInt(newBlock.hash, 16) >= newBlock.header.target) {
      throw 'Error: Invalid block: not valid proof'
    }

    return true
  }

  isChainValid() {
    if (!this.isValidGenesisBlock()) return false

    for (let i = 1; i < this.blockchain.chain.length; i++) {
      const currentBlock = this.blockchain.chain[i]
      const previousBlock = this.blockchain.chain[i - 1]
      
      let isValidBlock = this.validations(currentBlock, previousBlock)

      if (!isValidBlock) return false
    }
    
    return true
  }
}

function main() {
  let { blockchain } = bank = new Bank()

  console.log('----- Añadir dos bloques ----')

  blockchain.mempool.push(new Transaction('miner', 'address1', 10, Utils.now()))

  const block1 = {
    index: blockchain.getLastBlock().header.index + 1,
    timestamp: Utils.now(),
    transactions: blockchain.mempool
  }
  blockchain.addBlock(new Block({ index: block1.index, timestamp: block1.timestamp }, block1.transactions))
  
  blockchain.mempool.push(new Transaction('address1', 'address2', 10, Utils.now()))

  const block2 = {
    index: blockchain.getLastBlock().header.index + 1,
    timestamp: Utils.now(),
    transactions: blockchain.mempool
  }
  blockchain.addBlock(new Block({ index: block2.index, timestamp: block2.timestamp }, block2.transactions))
  console.log(bank.isChainValid() ? 'Blockchain valida' : 'Blockchain NO valida')

  console.log('----- Bad signature ----')

  blockchain.mempool.push(new Transaction('address2', 'address1', 10, Utils.now()))
  
  const block3 = {
    index: blockchain.getLastBlock().header.index + 1,
    timestamp: Utils.now(),
    transactions: blockchain.mempool
  }
  const nonBankPrivkey = Crypto.randomBytes(32)
  const nonBanPubkey = secp256k1.publicKeyCreate(nonBankPrivkey)
  const nonBankPubkeyHex = nonBanPubkey.toString('hex')

  blockchain.addBlock(new Block({ index: block3.index, timestamp: block3.timestamp }, block3.transactions, bank.bankPubKey), nonBankPrivkey)
  console.log(bank.isChainValid() ? 'Blockchain valida' : 'Blockchain NO valida')

  console.log('----- Not bank signature ----')

  blockchain.addBlock(new Block({ index: block3.index, timestamp: block3.timestamp }, block3.transactions, nonBankPubkeyHex), nonBankPrivkey)
  console.log(bank.isChainValid() ? 'Blockchain valida' : 'Blockchain NO valida')
  console.log('chain', JSON.stringify(blockchain.chain, null, 4))
 
  /* miner    => +50 => +40 => +50 = 140 
  * address1  =>  0  => +10 => -10 = 0
  * address2  =>  0  =>  0  => +10 = 10
  */
  
  console.log('Balances are:')
  console.log('-- bank:     ', blockchain.getBalance('miner'))
  console.log('-- address1: ', blockchain.getBalance('address1'))
  console.log('-- address2: ', blockchain.getBalance('address2'))
}

main()
