### Material del píloto del curso de Bitcoin y Blockchain en javascript.

## Cadenas de ejemplo

- ```BasicCoin.js```: Código base. Clase Block, Transaction y Blockchain. Capacidad de verificar la cadena.
- ```ECDSACoin.js```: Se añaden firmas digitales a los bloques de momento para simular un ente centralizado, monopoleo o banco
- ```BasicPoWCoin.js```: Proof Of Work básico, recompensas y mempool.
- ```UTXOCoin.js```: Cadena de bloques con UTXO-input/output P2PKH, transacciones firmadas, fees para el minero y genesis block desde fichero con nonce válido.

## Como ejecutar los ejemplos
# [Pre-requisito] Instalar cliente de git, curl e python
- ```sudo apt-get install git curl python```

# [Pre-requisito] Instalar nodejs
- ```curl -o- https://raw.githubusercontent.com/creationix/nvm/v0.33.11/install.sh | bash```
- Cerrar sesión y volver a iniciarla. Apartir de ahora los comandos nvm estarán disponibles.
- ```nvm install 10.15```
- ```nvm alias default 10.15```

# Ejecutar los ejemplos
- ```git clone https://gitlab.com/aitoribanez/curso-bitcoin-blockchain-piloto```
- ```cd curso-bitcoin-blockchain-piloto```
- ```npm install```
- ```node BasicCoin.js``` / ```node ECDSACoin.js``` / ```node BasicPoWCoin.js```
- UTXOCoin.js: ```npm run test```

# Instalación del software con Docker:


1. Arrancar un contenedor con nodejs:
   * Windows:
    ```docker run -it --rm -v "%cd%":/host -w /host -p 3000:3000 --entrypoint /bin/bash node```
   * Linux/Unix:
    ```docker run -it --rm -v "$(pwd)":/host -w /host -p 3000:3000 --entrypoint /bin/bash node```
    
    El contenido del directorio `/host` del contenedor queda mapeado a la carpeta desde donde hemos ejecutado el comando, de tal manera que todo el software propio del curso podrá ser editado desde el sistema host (anfitrión).

1. Ya dentro del contenedor, descargar el software del curso e instalarlo:
    ```
    git clone https://gitlab.com/aitoribanez/curso-bitcoin-blockchain-piloto
    cd curso-bitcoin-blockchain-piloto
    npm install express
    ```