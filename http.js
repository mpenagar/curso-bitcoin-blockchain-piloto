const express = require('express')
const bodyParser = require('body-parser')

class Http {
  constructor (blockchain) {
    let app = express()
    
    app.use(bodyParser.urlencoded({ extended: true }))
    app.use(bodyParser.json())
        
    app.get('/mempool', async function (req, res) {
      res.json(blockchain.mempool)
    })
        
    app.get('/utxo', async function (req, res) {
      res.json(blockchain.utxo)
    })
        
    app.get('/chain', async function (req, res) {
      res.json(blockchain.chain)
    })
        
    app.listen(3000, function () {
      console.log('HTTP Server listening on http://localhost:3000')
    })
  }
}

module.exports = Http
