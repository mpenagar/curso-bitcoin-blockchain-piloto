const Crypto = require('crypto')
const Utils = require('./Utils')
const genesisTimestamp = Utils.now()

class Block {
  constructor (header, transactions) {
    this.header = {}
    this.header.index = header.index
    this.header.timestamp = header.timestamp
    this.header.previousHash = ''
    this.transactions = transactions

    this.hash = this.calculateHash()
  }

  calculateHash() {
    const str = JSON.stringify(this.header)
    
    return Crypto.createHash('sha256').update(str).digest('hex')
  }
}

class Transaction {
  constructor(from, to, amount) {
    this.from = from
    this.to = to
    this.amount = amount
  }
}

class Blockchain {
  constructor() {
    this.chain = []

    const genesisBlock = this.createGenesisBlock()
    this.chain.push(genesisBlock)
  }

  createGenesisBlock() {
    const genesisTx = new Transaction(null, 'miner', 50)
    
    return new Block({ index: 0, timestamp: genesisTimestamp }, [genesisTx])
  }

  getLastBlock() {
    return this.chain[this.chain.length - 1]
  }

  addBlock(newBlock) {
    newBlock.header.previousHash = this.getLastBlock().hash
    newBlock.hash = newBlock.calculateHash()

    if (this.isBlockValid(newBlock)) {
      this.chain.push(newBlock)
    } else {
      console.log('Bloque no válido. Se olvida. No se almacena')
    }
  }

  isBlockValid(newBlock) {
    const previousBlock = this.getLastBlock()

    return this.validations(newBlock, previousBlock)
  }

  validations(newBlock, previousBlock) {
    try { 
      this.isValidBlockHash(newBlock)
      return this.isValidPreviousHash(newBlock, previousBlock)
     } catch (err) {
      console.log('ERROR', err)
      return false
    }
  }

  isValidGenesisBlock() {
    const memoryGenesisBlock = JSON.stringify(this.chain[0])
    const genesisBlock = JSON.stringify(this.createGenesisBlock())
    
    if (memoryGenesisBlock !== genesisBlock) {
      throw 'Error: Invalid block: genesis block'
    }

    return true
  }

  isValidPreviousHash(newBlock, previousBlock) {
    if (newBlock.header.previousHash !== previousBlock.calculateHash()) {
      throw 'Error: Invalid block: hashPreviousBlock'
    }

    return true
  }

  isValidBlockHash(newBlock) {
    if (newBlock.hash !== newBlock.calculateHash()) {
      throw 'Error: Invalid block: block hash'
    }

    return true
  }

  isChainValid() {
    if (!this.isValidGenesisBlock()) return false

    for (let i = 1; i < this.chain.length; i++) {
      const currentBlock = this.chain[i]
      const previousBlock = this.chain[i - 1]

      let isValidBlock = this.validations(currentBlock, previousBlock)

      if (!isValidBlock) return false
    }
    
    return true
  }
}

function main() {
  let blockchain = new Blockchain()

  // Add block's and validated it
  console.log('----- Añadir dos bloques ----')
  
  const block1 = {
    index: blockchain.getLastBlock().header.index + 1,
    timestamp: Utils.now(),
    transactions: [new Transaction('miner', 'address1', 10)]
  }
  blockchain.addBlock(new Block({ index: block1.index, timestamp: block1.timestamp }, block1.transactions))

  const block2 = {
    index: blockchain.getLastBlock().header.index + 1,
    timestamp: Utils.now(),
    transactions: [new Transaction('address1', 'address2', 10)]
  }
  blockchain.addBlock(new Block({ index: block2.index, timestamp: block2.timestamp }, block2.transactions))
  
  console.log(blockchain.isChainValid() ? 'Blockchain valida' : 'Blockchain NO valida')
  console.log(JSON.stringify(blockchain, null, 4))
  
  // changing block #1 and validating it
  blockchain.chain[1].header.index = 100
  blockchain.chain[1].hash = blockchain.chain[1].calculateHash()
  console.log('Bloque #1 despues del cambio: ', blockchain.chain[1])
  
  console.log(blockchain.isChainValid() ? 'Blockchain valida' : 'Blockchain NO valida')
}

main()
