const bs58 = require('bs58')
const ripemd160 = require('ripemd160')
const Crypto = require('crypto')
const secp256k1 = require('secp256k1')

class Utils {
  static sha256(data) {
    return Crypto.createHash('sha256').update(data).digest('hex')
  }
  
  static toRipemd160(pubKey) {
    return new ripemd160().update(pubKey).digest('hex')
  }
  
  static hexToByte(str) {
    if (!str) {
      return new Uint8Array()
    }
      
    let a = []
    for (let i = 0, len = str.length; i < len; i+=2) {
      a.push(parseInt(str.substr(i,2),16))
    }
      
    return new Uint8Array(a)
  }

  static byteToBase58(byte) {
    return bs58.encode(byte)
  }
    
  static toBase58Check(version, privKeyHex) {
    let privKeyAndVersion = version + privKeyHex
      
    let firstSHA = this.sha256(this.hexToByte(privKeyAndVersion))
    let secondSHA = this.sha256(firstSHA)
    let checksum = secondSHA.substr(0, 8).toUpperCase()
      
    // Añadir checksum al final de la clave privada y la versión, privateKeyAndVersion
    let keyWithChecksum = privKeyAndVersion + checksum;
    let privateKeyWIF = this.byteToBase58(Buffer.from(this.hexToByte(keyWithChecksum), 'hex'))
      
    return privateKeyWIF
  }
  
  // http://gobittest.appspot.com/Address (from step 3 does not work as expected)
  static getBitcoinAddress(pubkey) {
    let sha256_1pubkey = this.sha256(pubkey)
    let pubkey_ripemd160 = this.toRipemd160(sha256_1pubkey);
    // https://en.bitcoin.it/wiki/List_of_address_prefixes
    let version = '00'; //MainNet address. First byte, version is 1.
    // let version = '6F' // TestNet address. First byte, version is m or n.
    return this.toBase58Check(version, pubkey_ripemd160);
  }
  
  
  static bs58decodeNice(bs58Check) {
    return {
      'version': bs58Check.substr(0, 2),
      'pubkeyhash': bs58Check.substr(2, 40),
      // suma de comprobación de deteccion de errores para detectar errores tipograficos
      'last': bs58Check.substr(42, 48)
    }
  }
  
  static fromPubKeyToPubKeyHash(pubkey) {
    let pkSha256 = this.sha256(pubkey)
    return this.toRipemd160(pkSha256)
  }
  
/*   static addressToPublicKeyHash(address) {
    return this.bs58decodeNice(bs58.decode(address).toString('hex'))
  } */
  
  static newAddress() {
    const privkey = Crypto.randomBytes(32)
    const pubkey = secp256k1.publicKeyCreate(privkey)
    const address = Utils.getBitcoinAddress(pubkey)     
    
    return { privkey, pubkey, address }
  }

  static makeBankAnd2address() {
    /* address => pubkeyhash => pubkey
    bank  19sMGGesVr1J62LguDuxHVNaqpKogzzsYy => 6146b8234f71463122d032accd15c6925fa0c4f2 => 02f760b04feb4f5d70b96129c06a4987d416ee675749e0d665c4142609ead620b4
    alice 1HL13UT3tN8UcAm1x25wLHoPjw6FoYF6kF => b319f38fa43b1b82bcb4887bfcaf47693bae3b6e => 03ff17b5655f9cce07e29331eab03fb306e138e2257700159169da2dd4a4aa6718
    bob   1HgEx6XiLEdXBgBaRRJ8PVW7pXEHrcaFUZ => b6ede13e497e83ba77ce635174c355babe711b9f => 03d55c2f99899032d5f126b3b496fe1edfdeb9949a3f578557790a91954b5c453e */
    
    let bank = {}
    let alice = {}
    let bob = {}

    bank.privkey = Buffer.from(require('./bankWallet.json').privkey, 'hex')
    bank.pubkey = Buffer.from(secp256k1.publicKeyCreate(bank.privkey), 'hex')
    bank.address = this.getBitcoinAddress(bank.pubkey.toString('hex'))
   
    const aliceprivkey = Buffer.from('edf2334fbd11bea90c9b7a208a504467251f9bcbf7eeb4832ad34b6b2d564f6e', 'hex')
    const alicepubkey = secp256k1.publicKeyCreate(aliceprivkey) // compressed pubkey
      
    alice.privkey = aliceprivkey.toString('hex')
    alice.pubkey = alicepubkey.toString('hex')
    alice.address = this.getBitcoinAddress(alice.pubkey)
      
    const bobprivkey = Buffer.from('43f91d8553bc1945a8779fa778060cb7d4b6badf86c4256cdff0411c5aaffe90', 'hex')
    const bobpubkey = secp256k1.publicKeyCreate(bobprivkey)
      
    bob.privkey = bobprivkey.toString('hex')
    bob.pubkey = bobpubkey.toString('hex')
    bob.address = this.getBitcoinAddress(bob.pubkey)
    
    return { bank, alice, bob }
  }
    
  static now() {
    // return new Date().getTime()
    return Math.round(new Date() / 1000)
  }
}

module.exports = Utils
